function samplesFtrVal = getHogFtr_ant(samples)

% $Description:
%    -Compute the feature

% $Agruments
% Input;
%    -iH: inpute integral image
%    -samples: sample templates.samples.sx:x coordinate vector, samples.sy:
%    y coordinate vector
%    -ftr: feature template. ftr.px,ftr.py,ftr.pw,ftr.ph,ftr.pwt
% Output:
%    -samplesFtrVal: size: M x N, where M is the number of features, N is
%    the number of samples
% $ History $
%   - Created by Shabaz B Patel, on May 19th, 2015
%   - Revised by Kaihua Zhang, 10/12/2011
%size(samples,4)


%if (size(samples,4) ~= 1)
%fprintf('Size of samples')
%size(samples)
cellSize = 8;
for i=1:1:size(samples,4)
     imgR = samples(:,:,:,i);
     data = imresize(imgR, [32 32]);
       
     hog1 = vl_hog(single(data),cellSize);
     hog1 = reshape(hog1,4*4*31,1);
     features(:,i) = hog1;

end   
samplesFtrVal = features; %feature without preprocessing
