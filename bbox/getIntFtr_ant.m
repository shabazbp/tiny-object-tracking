function samplesFtrVal = getIntFtr_ant(samples)

% $Description:
%    -Compute the feature

% $Agruments
% Input;
%    -iH: inpute integral image
%    -samples: sample templates.samples.sx:x coordinate vector, samples.sy:
%    y coordinate vector
%    -ftr: feature template. ftr.px,ftr.py,ftr.pw,ftr.ph,ftr.pwt
% Output:
%    -samplesFtrVal: size: M x N, where M is the number of features, N is
%    the number of samples
% $ History $
%   - Created by Shabaz B Patel, on May 19th, 2015
%size(samples,4)


%if (size(samples,4) ~= 1)
%fprintf('Size of samples')`
features = zeros(100,size(samples,4));

for i=1:1:size(samples,3)
     imgR = samples(:,:,i);
     data = imresize(imgR, [10 10]);
     B = mean2(data);
     data = im2double(data) - B;
     data = reshape(data,[100,1]); 
     features(:,i) = data;
     
end   
samplesFtrVal = im2double(features); %feature without preprocessing
