function samples = sampleImg_Train(img,initstate,maxnum,posDataImg,data_pos)
% $Description:
%    -Compute the coordinate of sample image templates
% $Agruments
% Input;
%    -img: inpute image
%    -initistate: [x y width height] object position 
%    -inrad: outside radius of region
%    -outrad: inside radius of region
%    -maxnum: maximal number of samples
% Output:
%    -samples.sx: x coordinate vector,[x1 x2 ...xn]
%    -samples.sy: y ...
%    -samples.sw: width ...
%    -samples.sh: height...
% $ History $
%   - Created by Kaihua Zhang, on April 22th, 2011
%   - Revised by Kaihua Zhang, on May 25th, 2011

% rand('state',0);%important

%   load data_pos


x = initstate(1);
y = initstate(2);
w = initstate(3);
h = initstate(4);

if (size(data_pos,4) < 40)
    data_pos = posDataImg;
end
    
data = img(y-w/2:y+w/2-1,x-h/2:x+h/2-1,:);

length = size(data_pos,4);
if (length < maxnum && size(data,1) <= 40)
%    data_size = size(data)
%    data_pos_size = size(data_pos)
   data_pos = cat(4,data_pos,data);
else if (size(data,1) <= 40)
   data_pos(:,:,:,2:length) = data_pos(:,:,:,1:length-1);
   data_pos(:,:,:,1) = data;
    end
end
save('data_pos','data_pos');     
samples = data_pos;
