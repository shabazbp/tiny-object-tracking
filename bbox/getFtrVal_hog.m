function samplesFtrVal = getFtrVal_hog(img,samples,net,dataMean)
data = cell(size(samples,4),1);

cellSize = 8;
for i=1:1:size(samples,4)
     imgR = samples(:,:,:,i);
     data{i} = imresize(imgR, [32 32]);

     hog1 = vl_hog(single(data{i}), cellSize) ;
     hog1=reshape(hog1,4*4*31,1);     
     features(:,i)=hog1;
end   
 samplesFtrVal = features; %feature without preprocessing