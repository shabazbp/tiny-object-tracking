function samplesFtrVal = getNCCFtr_ant(I,J,tld,samples)

% $Description:
%    -Compute the feature

% $Agruments
% Input;
%    -iH: inpute integral image
%    -samples: sample templates.samples.sx:x coordinate vector, samples.sy:
%    y coordinate vector
%    -ftr: feature template. ftr.px,ftr.py,ftr.pw,ftr.ph,ftr.pwt
% Output:
%    -samplesFtrVal: size: M x N, where M is the number of features, N is
%    the number of samples
% $ History $
%   - Created by Shabaz B Patel, on May 19th, 2015



imgPrev = subsref(img_get(tld.source,I),struct('type','()','subs',{{1,1}}));
%imgPrev = repmat(imgPrev.input,[1 1 3]);
imgPrev = imgPrev.input;
centerPrev = bb_center(tld.bb(:,I));


try 
    imgPrevPatch = imgPrev(centerPrev(2,1)-20:centerPrev(2,1)+19,centerPrev(1,1)-20:centerPrev(1,1)+19);
catch 
    imgPrev = subsref(img_get(tld.source,1),struct('type','()','subs',{{1,1}}));    
    imgPrev = repmat(imgPrev.input,[1 1 3]);
    centerPrev = bb_center(tld.bb(:,1));
    imgPrevPatch = imgPrev(centerPrev(2,1)-20:centerPrev(2,1)+19,centerPrev(1,1)-20:centerPrev(1,1)+19);
end

imgPrevPatch = imresize(imgPrevPatch, [32 32]);
for i=1:1:size(samples,3)
     imgR = samples(:,:,i);
     data = imresize(imgR, [32 32]);
     data = data - imgPrevPatch;
     features(:,i) = sum(sum(data(:,:)));
end   
samplesFtrVal = features; %feat`ure without preprocessing
