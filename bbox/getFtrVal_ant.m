function samplesFtrVal = getFtrVal_ant(samples)

% $Description:
%    -Compute the feature
load /afs/.ir.stanford.edu/users/s/h/shabaz/CS231n/project/matconvnet-1.0-beta8/examples/data_ant_new/ant-baseline/net-epoch-17
load /afs/.ir.stanford.edu/users/s/h/shabaz/CS231n/project/matconvnet-1.0-beta8/examples/dataMean
run('/afs/.ir.stanford.edu/users/s/h/shabaz/CS231n/project/matconvnet-1.0-beta8/matlab/vl_setupnn.m');

% $Agruments
% Input;
%    -iH: inpute integral image
%    -samples: sample templates.samples.sx:x coordinate vector, samples.sy:
%    y coordinate vector
%    -ftr: feature template. ftr.px,ftr.py,ftr.pw,ftr.ph,ftr.pwt
% Output:
%    -samplesFtrVal: size: M x N, where M is the number of features, N is
%    the number of samples
% $ History $
%   - Created by Kaihua Zhang, on April 22th, 2011
%   - Revised by Kaihua Zhang, 10/12/2011
%size(samples,4)


%if (size(samples,4) ~= 1)
%fprintf('Size of samples')
%size(samples)
for i=1:1:size(samples,4)
     imgR = samples(:,:,:,i);
     %imgKaSize = size(imgR)
     data{i} = imresize(imgR, [32 32]);
end   
     data = single(cat(4, data{:}));
     data = bsxfun(@minus, data, dataMean);
     %Features from CNN Architecture
     net.layers{end}.class = ones(1,size(data,4));
     res=[];
     res = vl_simplenn(net, data, [], res, ...
              'disableDropout', true);
for i = 1:1:size(samples,4)
     features(:,i)=squeeze(res(end-2).x(:,:,:,i));
end
% else
%     features = [];
% end
 samplesFtrVal = features; %feature without preprocessing
