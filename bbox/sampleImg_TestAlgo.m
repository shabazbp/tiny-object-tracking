function samples = sampleImg_Test(img,initstate)
% $Description:
%    -Sample image templates for test
% $Agruments
% Input;
%    -img: inpute image
%    -initistate: [x y width height] object position 
% Output:
%    -samples.sx: x coordinate vector,[x1 x2 ...xn]
%    -samples.sy: y ...
%    -samples.sw: width ...
%    -samples.sh: height...
% $ History $
%   - Created by Shabaz Patel, on May 18th, 2015



x = floor(initstate(1,:));
y = floor(initstate(2,:));
w = 10;
h = 10;


if (y(1,1)-w/2 >=1 && y(1,1)+w/2-1 <= size(img,1) && x(1,1)-h/2 >=1 && x(1,1)+h/2-1 <= size(img,2))              
    data = img(y(1,1)-w/2:y(1,1)+w/2-1,x(1,1)-h/2:x(1,1)+h/2-1);
    samples = data;
else 
    samples = [];
end


for i = 2:1:size(x,2)
    if ( y(1,i)-w/2 >=1 && y(1,i)+w/2-1 <= size(img,1) && x(1,i)-h/2 >=1 && x(1,i)+h/2-1 <= size(img,2))
      data = img(y(1,i)-w/2:y(1,i)+w/2-1,x(1,i)-h/2:x(1,i)+h/2-1);
      if (size(samples,1) ~= 0)
      samples = cat(3,samples,data);
      else 
          samples = data;
      end
    end
end


