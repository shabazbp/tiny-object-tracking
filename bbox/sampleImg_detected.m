function [samples,xCoordinate,yCoordinate] = sampleImg_detected(img,img_name,initstate,radius,maxnum,flag_test,negData)
% rand('state',0);%important
%load(['pts_out_algos/pts_algo' num2str(img_name,'%06d') '.mat']);
load x_gt
load y_gt
% load negData
x = initstate(1);
y = initstate(2);
w = 40;
h = 40;
% x_detected = pts_result_algo(1,:);
% y_detected = pts_result_algo(2,:);
x_detected = x_gt{img_name};
y_detected = y_gt{img_name};
%size(x_detected)
if (isequal(flag_test,0))
   x_prob = floor(negData(1,:));
   y_prob = floor(negData(2,:));
else
   x_prob = [];
   y_prob = [];
end
xCoordianate = [];
yCoodrdinate = [];
siz = size(x_detected,2);
for i=1:1:siz%e(pts_result_algo,2)
    diff = [x_detected(i) - x,y_detected(i) - y];
    dist = norm(diff,2);
    if (isequal([x,y],[x_detected(i),y_detected(i)]) || isequal(flag_test,1))
        if (dist < radius)
           x_prob = [x_prob, x_detected(i)];
           y_prob = [y_prob, y_detected(i)];
        end
    end
end
data_detected = [];
if (numel(x_prob) ~= 0) 
data_detected = img(floor(y_prob(1))-w/2:floor(y_prob(1))+w/2-1,floor(x_prob(1))-h/2:floor(x_prob(1))+h/2-1,:);
xCoordinate = x_prob;
yCoordinate = y_prob;
for i=2:1:size(x_prob,2)
 if (x_prob(1,i)-w/2 >= 0 && x_prob(1,i)+w/2 -1<= size(img,1) && y_prob(1,i)-h/2 >=0 && y_prob(1,i) + h/2 -1 <= size(img,2)) 
     data_d = img(floor(y_prob(1,i))-w/2:floor(y_prob(1,i))+w/2-1,floor(x_prob(1,i))-h/2:floor(x_prob(1,i))+h/2-1,:);
     data_detected = cat(4,data_detected,data_d);
     xCoordinate = [xCoordinate, x_prob(1,i)];
     yCoordinate = [yCoordinate, y_prob(1,i)];
 end
end
save('data_detected','data_detected');
end
samples = data_detected;
