%%Code for tracking using CNN

function BB2 = Runtracker_ant(I,J,tld,img,bb)


  %%% Code for final decision of which sample it is
    %%%%-For actual flow Image

    test_sampleImage = sampleImg_Test(img,bb);  
    %testFeatures = getFtrVal_ant(test_sampleImage); 
    %testFeatures = getHogFtr_ant(test_sampleImage);
    %testFeatures = getIntFtr_ant(test_sampleImage);
    testFeatures = getNCCFtr_ant(I,J,tld,test_sampleImage);
    %------------------------------------
    %[label,score] = predict(tld.svmModel,testFeatures');% compute the classifier for all samples
    %index = find(score == min(score(:,1)));
    %fprintf('testFeatures')
    %size(testFeatures)
    index = find(testFeatures == min(testFeatures));
    
    if (size(index,1) < 50)
       x_best = bb(1,index(1));
       y_best = bb(2,index(1));
    else
       fprintf('Ayio');
       r = randi([1, size(index,1)],1,1);
       x_best = bb(1,index(r));
       y_best = bb(2,index(r));
    end 

 
BB2 = [x_best-20,y_best-20,x_best+20,y_best+20]';

