% Copyright 2011 Zdenek Kalal
%
% This file is part of TLD.
% 
% TLD is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% TLD is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with TLD.  If not, see <http://www.gnu.org/licenses/>.

function tld = tldProcessFrame_combine(tld,i)

I = tld.source.idx(i); % get current index
tld.img{I} = img_get(tld.source,I); % grab frame from camera / load image

% TRACKER using CNN ----------------------------------------------------------------

[tBB tConf tValid tld] = tldTrackingAlgo(tld,tld.bb(:,I-1),I-1,I); % frame-to-frame tracking (MedianFlow)
%tBB
%tConf
%tValid
%tld

% TRACKER using tld method -------------------------------------------------
[tBB_ex tConf_ex tValid_ex tld] = tldTrackingCNN(tld,tld.bb(:,I-1),I-1,I); % frame-to-frame tracking (MedianFlow)

% DETECTOR ----------------------------------------------------------------

[dBB dConf tld] = tldDetection(tld,I); % detect appearances by cascaded detector (variance filter -> ensemble classifier -> nearest neightbour)

% INTEGRATOR --------------------------------------------------------------

DT = 0; if isempty(dBB), DT = 0; end % is detector defined?
TR = 1; if isempty(tBB), TR = 0; end % is tld tracker defined?
TR_ex = 1; if isempty(tBB_ex), TR_ex = 0; end % is HOG Tracker defined?

% if (~TR || i < 10)
%      fprintf('Check1');
%      tld.bb(:,I) = tBB_ex;
%      tld.conf(I) = tConf_ex;
%      tld.size(I) = 1;
%      tld.valid(I) = tValid_ex;
% 
%      if DT % if detections are also defined
%         
%         [cBB,cConf,cSize] = bb_cluster_confidence(dBB,dConf); % cluster detections
%         id = bb_overlap(tld.bb(:,I),cBB) < 0.5 & cConf > tld.conf(I); % get indexes of all clusters that are far from tracker and are more confident then the tracker
%         
%         if sum(id) == 1 % if there is ONE such a cluster, re-initialize the tracker
%             
%             tld.bb(:,I)  = cBB(:,id);
%             tld.conf(I)  = cConf(:,id);
%             tld.size(I)  = cSize(:,id);
%             tld.valid(I) = 0; 
%           
%                 
%             
%         else % othervide adjust the tracker's trajectory
%             
%             idTr = bb_overlap(tBB,tld.dt{I}.bb) > 0.7;  % get indexes of close detections   
%             tld.bb(:,I) = mean([repmat(tBB,1,10) tld.dt{I}.bb(:,idTr)],2);  % weighted average trackers trajectory with the close detections
%             
%         end         
%     end

    if TR_ex % if tracker is defined
    %fprintf('Check2');
    % copy tracker's result
    tld.bb(:,I)  = tBB_ex;
    tld.conf(I)  = tConf_ex;
    tld.size(I)  = 1;
    tld.valid(I) = tValid_ex;
    tld.decide(I) = 1;
    if TR % if detections are also defined
     %if tracking by HOG
            %[cBB_ex,cConf_ex,cSize_ex] = bb_cluster_confidence(tBB,tConf); %Cluster prediction from HOG
            %if I < 30
            id_ex = tConf > tld.conf(I) % bb_overlap(tld.bb(:,I),cBB_ex) < 0.5 &  cConf_ex > tld.conf(I)
            %else
            %    id_ex = tConf > tld.conf(I) + 0.1
            %end
            if sum(id_ex) == 1  && I > 40
               tld.bb(:,I) = tBB(:,id_ex);
               tld.conf(I) = tConf(:,id_ex);
               tld.size(I) =  1; %cSize_ex(:,id_ex);
               tld.valid(I) = tValid;
               tld.decide(I) = 2;
            end
      if DT  
        [cBB,cConf,cSize] = bb_cluster_confidence(dBB,dConf); % cluster detections
        id = bb_overlap(tld.bb(:,I),cBB) < 0.5 & cConf > tld.conf(I) % get indexes of all clusters that are far from tracker and are more confident then the tracker
        
        if sum(id) == 1 % if there is ONE such a cluster, re-initialize the tracker
            
            tld.bb(:,I)  = cBB(:,id);
            tld.conf(I)  = cConf(:,id);
            tld.size(I)  = cSize(:,id);
            tld.valid(I) = 0; 
            tld.decide(I) = 3;
                   
        else % othervide adjust the tracker's trajectory
            
            idTr = bb_overlap(tBB,tld.dt{I}.bb) > 0.7;  % get indexes of close detections   
            tld.bb(:,I) = mean([repmat(tBB,1,10) tld.dt{I}.bb(:,idTr)],2);  % weighted average trackers trajectory with the close detections
            
        end
   
    end
    
    else % if tracker is not defined and detector is defined
        
        [cBB,cConf,cSize] = bb_cluster_confidence(dBB,dConf); % cluster detections
        
        if length(cConf) == 1 % and if there is just a single cluster, re-initalize the tracker
            tld.bb(:,I)  = cBB;
            tld.conf(I)  = cConf;
            tld.size(I)  = cSize;
            tld.valid(I) = 0; 
            tld.decide(I) = 3;
        end
    end
% else
% 
%      if TR_ex %if tracking by HOG
%          fprintf('chalja');
%          [cBB_ex,cConf_ex,cSize_ex] = bb_cluster_confidence(tBB_ex,tConf_ex); %Cluster prediction from HOG
%          id_ex = bb_overlap(tld.bb(:,I),cBB_ex) < 0.5 & cConf_ex > tld.conf(I);
%          if sum(id_ex) == 1
%             tld.bb(:,I) = cBB_ex(:,id_ex);
%             tld.conf(I) = cConf(:,id_ex);
%             tld.size(I) = cSize_ex(:,id_ex);
%             tld.valid(I) = 0;
%           end
%          end
%        
%     end
end

% LEARNING ----------------------------------------------------------------
if tld.control.update_detector && tld.valid(I) == 1
    tld = tldLearning(tld,I);
end

% display drawing: get center of bounding box and save it to a drawn line
if ~isnan(tld.bb(1,I))
    tld.draw(:,end+1) = bb_center(tld.bb(:,I));
    if tld.plot.draw == 0, tld.draw(:,end) = nan; end
else
    tld.draw = zeros(2,0);
end

if tld.control.drop_img && I > 2, tld.img{I-1} = {}; end % forget previous image








