%Copyright 2011 Zdenek Kalal
%
% This file is part of TLD.
% 
% TLD is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% TLD is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with TLD.  If not, see <http://www.gnu.org/licenses/>.
function [BB2 Conf Valid tld] = tldDetectCNN(tld,BB1,I,J,net)
% Estimates motion of bounding box BB1 from frame I to frame J
try
load(['/afs/.ir.stanford.edu/users/s/h/shabaz/CS231n/CLG-TV-matlab/OpenTLD/pts_out_algos/pts_algo' num2str(J+1999,'%06d') '.mat']);
catch
    load(['/afs/.ir.stanford.edu/users/s/h/shabaz/CS231n/CLG-TV-matlab/OpenTLD/pts_out_algos/pts_algo' num2str(J+1998,'%06d') '.mat']);
    pts1 = pts_result_algo;
    load(['/afs/.ir.stanford.edu/users/s/h/shabaz/CS231n/CLG-TV-matlab/OpenTLD/pts_out_algos/pts_algo' num2str(J+2000,'%06d') '.mat']);
    pts2 = pts_result_algo;
    minLen = min(size(pts1,2),size(pts2,2));
    pts_result_algo = (pts1(:,1:minLen) + pts2(:,1:minLen))/2;
end

    % initialize output variables
BB2    = []; % estimated bounding 
Conf   = []; % confidence of prediction
Valid  = 0;  % is the predicted bounding box valid? if yes, learning will take place ...

if isempty(BB1) || ~bb_isdef(BB1), return; end % exit function if BB1 is not defined

% estimate BB2
% fprintf('BB1')
% size(BB1)
%BB1
xFI    = bb_points(BB1,10,10,5); % generate 10x10 grid of points within BB1 with

%%%%%%BB2 = Runtracker_ant(tld,img,xFI);Write for loop instead of this function
x_pos = [];
y_pos = [];

img = subsref(img_get(tld.source,J),struct('type','()','subs',{{1,1}}));
img = repmat(img.input,[1 1 3]);
for i=1:1:size(pts_result_algo,2)
    x = pts_result_algo(1,i);
    y = pts_result_algo(2,i);
    for j = 1:1:size(xFI,2)
        x1 = xFI(1,j);
        y1 = xFI(2,j);
        dist = norm([x1,y1] - [x,y],2);
        if (dist < 60)
           x_pos = [x_pos, x];
           y_pos = [y_pos, y];
        end
    end
end
if (length(x_pos) > 1)
   r = randi([1 length(x_pos)],1,1);
   x_Sel = x_pos(r);
   y_Sel = y_pos(r);
while(~(x_Sel-20>= 1 && y_Sel-20>=1 && x_Sel+20<=size(img,1) && y_Sel+20<=size(img,2)))
   r = randi([1 length(x_pos)],1,1);
   x_Sel = x_pos(r);
   y_Sel = y_pos(r);
end
else
   x_Sel = x_pos;
   y_Sel = y_pos;
end
if(length(x_Sel) == 1)
  BB2 = [x_Sel-20 , y_Sel-20, x_Sel+20, y_Sel+20]';
else
    BB2 = BB1;
end


% estimate confidence and validity
patchJ   = tldGetPattern(tld.img{J},BB2,tld.model.patchsize); % sample patch in current image
[~,Conf] = tldNN(patchJ,tld); % estimate its Conservative Similarity (considering 50% of positive patches only)

% Validity
Valid    = tld.valid(I); % copy validity from previous frame
if Conf > tld.model.thr_nn_valid, Valid = 1; end % tracker is inside the 'core'



