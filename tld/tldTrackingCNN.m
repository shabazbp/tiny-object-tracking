%Copyright 2011 Zdenek Kalal
%
% This file is part of TLD.
% 
% TLD is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% TLD is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with TLD.  If not, see <http://www.gnu.org/licenses/>.

function [BB2 Conf Valid tld] = tldTrackingCNN(tld,BB1,I,J,net)
% Estimates motion of bounding box BB1 from frame I to frame J
% initialize output variables
BB2    = []; % estimated bounding 
Conf   = []; % confidence of prediction
Valid  = 0;  % is the predicted bounding box valid? if yes, learning will take place ...

if isempty(BB1) || ~bb_isdef(BB1), return; end % exit function if BB1 is not defined

xFI    = bb_points(BB1,10,10,5); % generate 10x10 grid of points within BB1 with margin 5 px
%xFJ    = lk(2,tld.img{I}.input,tld.img{J}.input,xFI,xFI); % track all points by Lucas-Kanade tracker from frame I to frame J, estimate Forward-Backward error, and NCC for each point
%%Estimate all possible pathes around it by considering a distance of 10 pixels around the actual origi

img = subsref(img_get(tld.source,J),struct('type','()','subs',{{1,1}}));

%img = repmat(img.input,[1 1 3]);
img = img.input;
%try
BB2 = Runtracker_ant(floor(I),floor(J),tld,img,xFI);
%catch
%    BB2 = BB1;
%end

% estimate confidence and validity
patchJ   = tldGetPattern(tld.img{J},BB2,tld.model.patchsize); % sample patch in current image
[~,Conf] = tldNN(patchJ,tld); % estimate its Conservative Similarity (considering 50% of positive patches only)

% Validity
Valid    = tld.valid(I); % copy validity from previous frame
if Conf > tld.model.thr_nn_valid, Valid = 1; end % tracker is inside the 'core'



