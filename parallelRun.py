import unittest
import numpy as np
import nose.tools
import math
import subprocess
from tempfile import mkstemp
from shutil import move
from os import remove, close
import os
import itertools
import sys
from multiprocessing import Process
import time

def run(bb,i):
    
    file_path_init = './_input/init.txt'
    f = open( file_path_init, 'r' )
    lines = f.readlines()
    f.close()
    f = open( file_path_init,'w' )
    f.write( " ".join(itertools.imap(str, bb)))
    f.close()

    file_path_m = './tld/tldExample.m'
    
    replace(file_path_m,'x_final'+str(i-1),'x_final'+str(i))
    replace(file_path_m,'y_final'+str(i-1),'y_final'+str(i))
    replace(file_path_m,'conf'+str(i-1),'conf'+str(i))
    replace(file_path_m,'decide'+str(i-1),'decide'+str(i))
    
    
    #os.chdir('/Users/Shabaz/Desktop/Studies/Quarter_6/Project/Script')
    os.system("matlab -nojvm < run_TLD.m")
    #os.system("python tldExample.py " + str(i))
    

def replace(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,"wt") as new_file:
        with open(file_path,"rt") as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern,subst))
    close(fh)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

BB = [[948,180,988,220],[306,508,346,548],[572,527,612,567],[886,189,926,229],[815,357,855,397],[624,185,664,225],[659,555,699,595],[163,440,203,480],[567,415,607,455],[532,259,572,299],[536,521,576,561],[604,621,644,661]] 
i = 0
for bb in BB:
    i+=1
    pi = "p" + str(i)
    print pi
    pi = Process(target=run(bb,i))
    pi.start()
    time.sleep(10)


    

